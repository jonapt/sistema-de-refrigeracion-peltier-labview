# Descripcción


Se propone la implementación de un controlador Proporcional-Derivativo-Integral (PDI) 
para regular la temperatura en un sistema de refrigeración que utiliza celdas de Peltier. 
Este controlador ajusta la velocidad del ventilador de extracción de aire mediante modulación 
por ancho de pulso (PWM), lo que influye directamente en la cantidad de aire frío extraído y, 
por ende, en la temperatura del sistema. Se establece que la temperatura mínima estará determinada 
por la capacidad de enfriamiento de la celda, mientras que la temperatura máxima será la temperatura 
ambiente.

En este sistema de control, la variable de control será el PWM del ventilador, mientras que la 
variable de proceso será la temperatura interna del sistema refrigerado. El setpoint de temperatura
será ajustado según las necesidades específicas del sistema en cada momento. Además, se implementará 
el método de sintonización Lambda Tuning para el PID, lo que permitirá optimizar los parámetros del 
controlador y mejorar su respuesta frente a cambios en las condiciones del sistema.


## Descripción de variables

| Encabezado 1 | Descripción           |
|--------------|-----------------------|
| PV           | Variable de proceso   |
| CV           | PWM ventilador        |
| SP           | Setpoint              |

## Sintonía (Lambda Tunning) PID

| Encabezado 1 | Descripción           |
|--------------|-----------------------|
| Kp           |  7.48   |
| Ti           |  0.04   |
| Td           | -0.02   |
